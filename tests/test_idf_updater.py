#!/usr/bin/env python3
import sys
import os
from besos.besos_utils import get_file_name
from besos.eplus_funcs import get_idf_version_pth
from idf_updater.idf_updater import update_idf
from shutil import copyfile, move

import pytest


def return_original_files(idf, final_test=False):
    idf_path = os.path.join("tests", "data")
    copy_folder = os.path.join(idf_path, "original_idfs")
    if isinstance(idf, list):
        for file in idf:
            file_name = get_file_name(file)
            copy_version = os.path.join(copy_folder, file_name)
            copyfile(copy_version, file)
            if final_test:
                os.remove(copy_version)
    else:
        file_name = get_file_name(idf)
        copy_version = os.path.join(copy_folder, file_name)
        copyfile(copy_version, idf)
        if final_test:
            os.remove(copy_version)


def test_run_single():
    idf_path = os.path.join("tests", "data")
    testfile = os.path.join(idf_path, "testfile.idf")
    goal_version = "9.3"
    copy_orig_file = os.path.join(idf_path, "original_idfs", get_file_name(testfile))
    copyfile(testfile, copy_orig_file)
    update_idf(testfile, keep_orig=False)
    if goal_version == get_idf_version_pth(testfile):
        return_original_files(testfile)
    else:
        return_original_files(testfile)
        raise ValueError


def test_run_single_with_version():
    idf_path = os.path.join("tests", "data")
    testfile0 = os.path.join(idf_path, "testfile0.idf")
    copy_orig_file = os.path.join(idf_path, "original_idfs", get_file_name(testfile0))
    copyfile(testfile0, copy_orig_file)
    update_idf(testfile0, goal_version=9.0, keep_orig=False)
    if "9.0" == get_idf_version_pth(testfile0):
        return_original_files(testfile0)
    else:
        return_original_files(testfile0)
        raise ValueError


# TODO: Could add tests for keep_int and other arguments


def test_run_list():
    idf_path = os.path.join("tests", "data")
    testfile = os.path.join(idf_path, "testfile.idf")
    testfile0 = os.path.join(idf_path, "testfile0.idf")
    testfile1 = os.path.join(idf_path, "testfile1.idf")
    testfile2 = os.path.join(idf_path, "testfile2.idf")
    single_files = [testfile, testfile0, testfile1, testfile2]

    for file in single_files:
        copy_orig_file = os.path.join(idf_path, "original_idfs", get_file_name(file))
        copyfile(file, copy_orig_file)
    goal_version = "9.2"
    update_idf(single_files, goal_version=goal_version, keep_orig=False)
    for file in single_files:
        if goal_version != get_idf_version_pth(file):
            return_original_files(single_files)
            raise ValueError
    return_original_files(single_files)


def test_directory():
    idf_path = os.path.join("tests", "data")
    testfile = os.path.join(idf_path, "testfile.idf")
    testfile0 = os.path.join(idf_path, "testfile0.idf")
    testfile1 = os.path.join(idf_path, "testfile1.idf")
    testfile2 = os.path.join(idf_path, "testfile2.idf")
    single_files = [testfile, testfile0, testfile1, testfile2]

    goal_version = "8.8"
    update_idf(idf_path, goal_version=goal_version, keep_orig=False)
    for file in single_files:
        copy_orig_file = os.path.join(idf_path, "original_idfs", get_file_name(file))
        if goal_version == get_idf_version_pth(file):
            pass
        else:
            return_original_files(single_files)
            raise ValueError
    return_original_files(single_files, final_test=True)
