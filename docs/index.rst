*********************************
idf_updater documentation
*********************************
This module is used to update singular IDF's, lists of IDF's, or directories of IDF's to newer versions. Built
for Linux and MacOS as windows has the IDFVersionUpdater.exe functionality from
EnergyPlus.

Will find local energyplus conversion files. Currently supports version updating
from 8.0 --> newest EnergyPlus Version

'update_idf' can be passed a folder containing idfs, a path to a singular idf, or
a list of paths to idf files.

.. toctree::
	:maxdepth: 1
	:caption: idf_updater library
	:glob:
	:hidden:

	workflow


.. toctree::
	:maxdepth: 2
	:caption: Modules
	:glob:
	:hidden:

	modules/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Acknowledgement
==================
besos library and BESOS platform have been created by the Energy In Cities Group and University of Victoria.
