IDF Updater Workflow
====================

First import the update idf function

.. code:: ipython3

    from idf_updater.idf_updater import update_idf

Following imports,
~~~~~~~~~~~~~~~~~~

-  Obtain the path to the idf
-  Run update_idf with the path

This will keep the original version tagged with its version (in this
case below, ‘testfile-v8.8.idf’)

.. code:: ipython3

    idf_path = 'testfile.idf'
    update_idf(idf_path)


.. parsed-literal::

    Current Version: 9.3, Conversion Complete


We can also specify version with the goal_version argument as follows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    idf_path_2 = 'testfile-v8.8.idf'
    update_idf(idf_path_2, goal_version='9.0')

The update_idf function also accepts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  lists of paths
-  directories to search for idf files

First we will do a list of files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: ipython3

    idf_files = ['testfile1.idf','testfile2.idf']
    update_idf(idf_files)


.. parsed-literal::

    Current Version: 8.8
    Current Version: 8.9
    Current Version: 9.0
    Current Version: 9.1
    Current Version: 9.2
    Current Version: 9.3, Conversion Complete
    Current Version: 8.8
    Current Version: 8.9
    Current Version: 9.0
    Current Version: 9.1
    Current Version: 9.2
    Current Version: 9.3, Conversion Complete


Next a directory of files
^^^^^^^^^^^^^^^^^^^^^^^^^

This does have the option of doing a recursive search (defaults to
false) if you want to look in children folders for more files to update.

.. code:: ipython3

    idf_path = 'idf_folder'
    update_idf(idf_path)


.. parsed-literal::

    Current Version: 8.8
    Current Version: 8.9
    Current Version: 9.0
    Current Version: 9.1
    Current Version: 9.2
    Current Version: 9.3, Conversion Complete
    Current Version: 8.8
    Current Version: 8.9
    Current Version: 9.0
    Current Version: 9.1
    Current Version: 9.2
    Current Version: 9.3, Conversion Complete
