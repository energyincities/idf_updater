IDF Updater: Linux and MacOS IDF updating python package.
=========================================================

[![PyPI Latest Release](https://img.shields.io/pypi/v/idf-updater.svg)](https://pypi.org/project/idf-updater/)
[![PyPi Package format](https://img.shields.io/pypi/format/idf-updater)](https://pypi.org/project/idf-updater/)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[![Documentation](https://img.shields.io/readthedocs/idf-updater)](https://idf-updater.readthedocs.io/en/stable/)

[![pipeline](https://gitlab.com/energyincities/idf_updater/badges/master/pipeline.svg)](https://gitlab.com/energyincities/idf_updater/-/commits/master)
[![coverage](https://gitlab.com/energyincities/idf_updater/badges/master/coverage.svg)](https://gitlab.com/energyincities/idf_updater/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

This package is used to update singular IDF's, lists of IDF's, or directories of IDF's to newer versions. Built
for Linux and MacOS as windows has the IDFVersionUpdater.exe functionality from EnergyPlus.

Will find local EnergyPlus conversion files. Currently supports version updating from 8.0 --> newest EnergyPlus Version

'update_idf' can be passed a folder containing idf files, a path to a singular idf, or
a list of paths to idf files.

Install requires EnergyPlus installed in default location on computer.

Documentation can be found here: https://idf-updater.readthedocs.io/en/latest/
